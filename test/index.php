<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Live Crud</title>

        <!-- Bootstrap -->
       <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css" >

        <!-- Optional theme -->
        <link rel="stylesheet" href="css/bootstrap-theme.min.css" >

        <!-- Latest compiled and minified JavaScript -->

        <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>

    <?php include_once 'includes/connect.php'; ?>

        <header>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Crud System</a>
                    </div>

                </div><!-- /.container-fluid -->
            </nav>
        </header>
      
       
        <div class="container-fluid">
            <!-- Response message -->
            <div class="response-msg"></div>
            <!-- Search -->
             <div class="row">
                 <div class="col-md-2 col-md-offset-10">
                    <form id="form_search">
                        <div class="form-group">
                            <label for="search">Search</label>
                            <input type="text" class="form-control" name="search" id="search" placeholder="search">
                        </div>
                    </form>
                </div>
            </div>
           
            <div class="row">
                 <div class="col-md-6">
                    <h3>List</h3>
                    <hr>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>Birthdate</th>
                                <th>Address</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="result">
                        </tbody>
                    </table>
                </div>

                <div class="col-md-6">
                    <h3>Form</h3>
                    <hr>
                    <form id="user_form">
                       
                    </form>
                    <a href="" id="clearbtn" class="btn btn-default">Clear</a>

                </div>
            </div>
           
        </div>
        

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
    </body>


    <script type="text/javascript">
        $(document).ready(function(){
            $( "#birthdate" ).datepicker();
            loadstation();
            loadform();
            $("#user_form").submit(function(event){
                event.preventDefault();
                var data = $(this).serialize();
                $.ajax({
                    url: "postactions/save.php",
                    type: "post",
                    data: data,
                    success: function(msg){
                        loadstation();
                        $(".response-msg").html(msg);
                        $("#firstname").val('');
                        $("#lastname").val('');
                        $("#birthdate").val('');
                        $("#address").val('');
                    }
                });
            });

            $(".table").on("click", ".deletebtn", function(e){
                e.preventDefault();
                $.ajax({
                    url: $(this).attr("href"),
                    type: "GET",
                    success: function(msg){
                        loadstation();
                        loadform();
                        $(".response-msg").html(msg);
                    }

                });

            });

             $(".table").on("click", ".editbtn", function(e){
                e.preventDefault();
                $.ajax({
                    url: $(this).attr("href"),
                    type: "GET",
                    success: function(msg){
                        $("#user_form").html(msg);
                    }

                });

            });


             $("#clearbtn").click(function(e){
                e.preventDefault();
                loadform();
            });

            $('#search').on('input',function(e){
                $.ajax({
                    url: "postactions/get_users.php",
                    type: "POST",
                    data: $("#form_search").serialize(),
                    success: function(rtrn){
                        $("#result").html(rtrn);
                    }
                });
            });
                
             



        });

        function loadstation(){
            $("#result").load("postactions/get_users.php");
        }

        function loadform(){
            $("#user_form").load("postactions/form.php");
        }
    </script>

</html>