    <?php 

        $firstname = "";
        $lastname = "";
        $birthdate = "";
        $address = "";

        if($_GET){
            include_once '../includes/connect.php';
            $sql = "SELECT * FROM users WHERE id = '".$_GET['id']."'";
            $result = mysqli_query($conn, $sql);
            $row = mysqli_fetch_assoc($result);

            $firstname = $row['firstname'];
            $lastname = $row['lastname'];
            $birthdate = $row['birthdate'];
            $address = $row['address'];

        }
    ?>

    <div class="form-group">
        <label for="firstname">Firstname</label>
        <input type="text" class="form-control" id="firstname" name="firstname" value="<?php echo $firstname; ?>" placeholder="Firstname" required>
    </div>

    <div class="form-group">
        <label for="lastname">Lastname</label>
        <input type="text" class="form-control" id="lastname" name="lastname" value="<?php echo $lastname; ?>" placeholder="Lastname" required>
    </div>

    <div class="form-group">
        <label for="birthdate">Birthdate</label>
        <input type="date" class="form-control" id="birthdate" name="birthdate" value="<?php echo $birthdate; ?>" required>
    </div>

     <div class="form-group">
        <label for="address">Address</label>
        <textarea class="form-control" name="address" id="address" required><?php echo $address; ?></textarea>
    </div>

    <?php if($_GET){ ?>
        <input type="hidden" name="_method" value="update">
        <input type="hidden" name="user_id" value="<?php echo $_GET['id']; ?>">
    <?php }else{ ?>
        <input type="hidden" name="_method" value="add">
    <?php } ?>
    <button type="submit" class="btn btn-default">Save</button>